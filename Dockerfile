#===============================================================================#
# Archivo: Dockerfile
# Descripcion: Contenedor de Jenkins Agent, NodeJS y  Semantic release v21
# Version: v1.0
# Fecha: 11/05/2023 - Simon Castellanos - simon.castellanoschauran@nttdata.com
#==============================================================================#

# Usa la última
FROM  registry.redhat.io/openshift4/ose-jenkins-agent-base

# Instalacion de NodeJS 18
RUN dnf install -y gcc-c++ make curl sudo
RUN curl -sL https://rpm.nodesource.com/setup_18.x | sudo -E bash -

RUN dnf install nodejs -y
RUN node -v && npm -v

# Instalacion dependencias Semantic-Release
RUN npm i -g semantic-release && \
    npm i -g @semantic-release/changelog && \
    npm i -g @semantic-release/commit-analyzer && \
    npm i -g @semantic-release/exec && \
    npm i -g @semantic-release/git  && \
    npm i -g @semantic-release/gitlab && \
    npm i -g @semantic-release/npm  && \
    npm i -g @semantic-release/release-notes-generator
