Simon Castellanos NTT DATA

## [5.0.1](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/compare/v5.0.0...v5.0.1) (2023-04-24)


### Bug Fixes

* Agrega nueva linea para 5.0.1 ([9f36efc](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/commit/9f36efc7696e015f8edec2d4c87c18e016171c87))

# [5.0.0](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/compare/v4.0.1...v5.0.0) (2023-04-23)


### Code Refactoring

* **resource:** rename useguidedfailure argument on environment resource ([4536189](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/commit/4536189f7fe717823f495ad2c96ef5b8758accf0))


### BREAKING CHANGES

* **resource:** The useguidedfailure argument has been renamed use_guided_failure in the octopusdeploy_environment

## [4.0.1](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/compare/v4.0.0...v4.0.1) (2023-04-23)


### Bug Fixes

* Actualizacion de .releaserc.yml ([43c4e30](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/commit/43c4e300042317d14553d08d313ec7bbbb3512bc))

# [4.0.0](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/compare/v3.0.0...v4.0.0) (2023-04-23)


### Bug Fixes

* Ajustear cambios Github a GitLab ([441b44e](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/commit/441b44e48d3ba055d953ec3f3af4994d44add6f1))


### Code Refactoring

* **resource:** se agrega nueva linea a testfile1 ([c693fd4](https://gitlab.com/simon.castellanos.1/SemanticDemo_1/commit/c693fd4e08065104a0a0cc4bf29e6efe7b50547a))


### BREAKING CHANGES

* **resource:** Cambios para version 4

# [3.0.0](https://github.com/scastellanos77/SemanticDemo_1/compare/v2.0.0...v3.0.0) (2023-04-22)


### Code Refactoring

* **resource:** se agrega nueva linea a testfile1 ([aef6609](https://github.com/scastellanos77/SemanticDemo_1/commit/aef66092b83cbf0e7b301e80d99d3560b2cfc785))


### BREAKING CHANGES

* **resource:** Cambios para version 3

# [2.0.0](https://github.com/scastellanos77/SemanticDemo_1/compare/v1.1.1...v2.0.0) (2023-04-22)


### Code Refactoring

* **resource:** rename useguidedfailure argument on environment resource ([2721611](https://github.com/scastellanos77/SemanticDemo_1/commit/2721611406067b24f6622724bccc0e9e57ec5420))


### BREAKING CHANGES

* **resource:** The useguidedfailure argument has been renamed use_guided_failure in the octopusdeploy_environment

## [1.1.1](https://github.com/scastellanos77/SemanticDemo_1/compare/v1.1.0...v1.1.1) (2023-04-22)


### Bug Fixes

* Nueva linea en archivo testfile1 ([9f6c6cd](https://github.com/scastellanos77/SemanticDemo_1/commit/9f6c6cda38f7e031029c2f7d1a93ddaf66b3a97c))

# [1.1.0](https://github.com/scastellanos77/SemanticDemo_1/compare/v1.0.1...v1.1.0) (2023-04-22)


### Features

* Se agrega el archivo testfile1 ([bd0daa7](https://github.com/scastellanos77/SemanticDemo_1/commit/bd0daa7a44d9753a9ccfd5b0d4b5319bfe815185))

## [1.0.1](https://github.com/scastellanos77/SemanticDemo_1/compare/v1.0.0...v1.0.1) (2023-04-22)


### Bug Fixes

* Modificacion de parametros en releaserc.yml ([3b378a5](https://github.com/scastellanos77/SemanticDemo_1/commit/3b378a56349a0512c8acec1ad0e7ea5c780a678e))

# 1.0.0 (2023-04-22)


### Bug Fixes

* Ajustes menores en codigo ([5b39dbd](https://github.com/scastellanos77/SemanticDemo_1/commit/5b39dbdd2654d1c3debaa4a16f7ebed52c4c9939))
* Modificacion de releaserc.yml y Version.txt ([787fb35](https://github.com/scastellanos77/SemanticDemo_1/commit/787fb35e562154cfcc98f2801d46e43676f7f371))
